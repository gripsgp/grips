import numpy as np
import tensorflow as tf
from grips import kernels

def test_rbf_kernel():
    kernel = kernels.RBF(variance=1.0, lengthscale=1.0)

    x = tf.convert_to_tensor([
        [1.0],
        [2.0]
    ])
   
    with tf.Session() as sess:
        test = sess.run(tf.shape(x))
        k = sess.run(kernel(x, x))
    
    np.testing.assert_almost_equal(k, np.array([
        [1.0, 0.6065307],
        [0.6065307, 1.0],
    ]))

def test_sum_kernel():
    k_1 = kernels.RBF(variance=1.0, lengthscale=1.0)
    k_2 = kernels.RBF(variance=2.0, lengthscale=2.0)

    k = k_1 + k_2

    assert isinstance(k, kernels.SumKernel)
    assert k.first_kernel == k_1
    assert k.second_kernel == k_2
    
    x = tf.convert_to_tensor([
        [1.0],
        [2.0]
    ])

    with tf.Session() as sess:
        sum_k = sess.run(k(x, x))
        each_k = sess.run(k_1(x, x) + k_2(x, x))

    np.testing.assert_almost_equal(sum_k, each_k)

def test_prod_kernel():
    k_1 = kernels.RBF(variance=1.0, lengthscale=1.0)
    k_2 = kernels.RBF(variance=2.0, lengthscale=2.0)

    k = k_1 * k_2

    assert isinstance(k, kernels.ProdKernel)
    assert k.first_kernel == k_1
    assert k.second_kernel == k_2
    
    x = tf.convert_to_tensor([
        [1.0],
        [2.0]
    ])

    with tf.Session() as sess:
        sum_k = sess.run(k(x, x))
        each_k = sess.run(k_1(x, x) * k_2(x, x))

    np.testing.assert_almost_equal(sum_k, each_k)
