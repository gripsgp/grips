#!/usr/bin/env python

from distutils.core import setup
with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='grips',
      version='0.0.1',
      packages=['grips'],
      install_requires=requirements,
     )
