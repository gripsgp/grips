import autograd.numpy as np

def squared_distance(x, y):
    x_square = np.sum(np.square(x), axis=1, keepdims=True)
    y_square = np.sum(np.square(y.T), axis=0, keepdims=True)
    return x_square - 2 * np.dot(x, y.T) + y_square

def abs_distance(x, y):
    return np.sum(np.abs(x[:, np.newaxis] - y), axis=-1)

def rbf(x, y, lengthscale=1.0, variance=1.0):
    return variance * np.exp(-squared_distance(x, y) / (2 * lengthscale**2))

def matern32(x, y, lengthscale=1.0, variance=1.0):
    r = abs_distance(x, y)
    factor = np.sqrt(3) * r / lengthscale
    return variance * (1 + factor) * np.exp(-factor)

def linear(x, y, bias=1.0, variance=1.0):
    return bias + variance * np.dot(x, y.T)

