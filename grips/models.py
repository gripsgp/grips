import autograd.numpy as np
from autograd import grad
from scipy.optimize import minimize

def cholesky_invert(x):
    L = np.linalg.cholesky(x)
    L_inv = np.linalg.inv(L)
    return np.dot(L_inv.T, L_inv)

def log_normal_likelihood(x, mu, cov):
    scale = -0.5 * np.sum(np.log(np.diag(2 * np.pi * cov)))
    centered_x = x - mu.T # N x D
    precision = cholesky_invert(cov)
    return  scale - 0.5 * np.sum(np.dot(centered_x, precision) * centered_x, axis=1)

class GaussianGP:
    def __init__(self, get_parameters, start_params):
        self.get_parameters = get_parameters
        self.start_params = start_params
        self.x = None
        self.y = None

        self.set_parameters(self.start_params)

    def set_parameters(self, params):
        self.kernel, self.noise_var = self.get_parameters(params)
        self.params = params
        if self.x is not None:
            self.condition_on(self.x, self.y)

    def condition_on(self, x, y):
        self.x = x
        self.y = y
        self.K_xx = self.kernel(x, x) + self.noise_var * np.eye(len(x))
        self.K_xx_inv = cholesky_invert(self.K_xx) 

    def predict(self, new_x, latent=False):
        K_xn = self.kernel(self.x, new_x)
        K_nn = self.kernel(new_x, new_x) 
        if not latent:
            K_nn += self.noise_var * np.eye(len(new_x))
        
        mu = np.dot(np.dot(K_xn.T, self.K_xx_inv), self.y)
        cov = K_nn - np.dot(np.dot(K_xn.T, self.K_xx_inv), K_xn)
        return mu, cov

    def nlog_marginal_likelihood(self, kernel, noise_var):
        cov = kernel(self.x, self.x) + noise_var * np.eye(len(self.x))
        scale = -0.5 * np.sum(np.log(np.diag(2 * np.pi * cov)))
        precision = cholesky_invert(cov)
        return -(scale - 0.5 * np.dot(np.dot(self.y.T, precision), self.y))

    def optimize(self):
        def loss_function(params):
            kernel, noise_var = self.get_parameters(params)
            return self.nlog_marginal_likelihood(kernel, noise_var)

        grad_loss = grad(loss_function)
        x0 = self.start_params
        res = minimize(loss_function, jac=grad_loss, x0=x0, method='L-BFGS-B')

        self.set_parameters(res.x)
        return res.x
